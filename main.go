package main

import (
	"fmt"
	"io"
	"os"
	"bufio"
	"strconv"
	"errors"
)

var dataStore map[int]string

func main() {

	dataStore = make(map[int]string)

	bio := bufio.NewReader( os.Stdin )

	var err error
	var str string
	for err != io.EOF {
		str, err = bio.ReadString('\n')
		_, err := parseStr(str)
		if err != nil {
			fmt.Println(err)
		}
	}
	
}

func parseStr(input string) (int, error) {
	var cmd byte = 0
	var arg string
	var i = 0

	for i < len(input) {
		/* Excape character */
		if input[i] == '/' { 
			i++ 
		} else {
			/* No excape check if we are at command ending */
			if input[i] == cmd {
				return i+1, runCmd(cmd, arg)
			}
		}

		/* Parse character */
		if cmd == 0{
			cmd = input[i]
		} else {
			arg = arg + string(input[i])
		}
		/* Next value */
		i++
	}

	return i+1, nil

}

func runCmd(cmd byte, arg string) error {
	switch cmd {
		case 's':
			fmt.Printf("Saving %s in %d\n", arg, len(arg))
			dataStore[len(arg)] = arg
		case 'r':
			/* Parse index */
			index, err := strconv.ParseInt(arg, 10, 32)
			if err != nil {
				return err
			}

			/* Retrieve value */
			res, ok := dataStore[ int(index) ]
			if !ok {
				return errors.New("Value does not exist")
			}
			fmt.Println(res)
	}

	return nil
}
